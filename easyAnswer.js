/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

/**
 *  @version 2.0.0 (2018.04.24)
 *  @author BART96
 *  @name EasyAnswer
**/

const style = `<style type="text/css">
  #easyAnswer { position:absolute; right:100%; color:#05ff00; background:#000000aa; border:2px solid red; border-radius:10px; }
  #easyAnswer:before {content:''; position:absolute; border:10px solid transparent; border-left:22px solid red; top:calc(50% - 10px); left:100%; }

  #easyAnswer span { display:block; overflow-x:hidden; counter-reset:answer; border-radius:10px; }
  #easyAnswer span::-webkit-scrollbar { width:5px; }
  #easyAnswer span::-webkit-scrollbar-thumb { border-radius:0 99px 99px 0; background-color:#37d0ff; border:none; }
  #easyAnswer span::-webkit-scrollbar-track-piece { border-radius:0 99px 99px 0; background-color:#000000; margin:3px 0; }

  #easyAnswer span div { padding:3px; max-height:64px; overflow:hidden; font-size:14px; line-height:16px; }
  #easyAnswer span div:first-child { border-radius:10px 10px 0 0; }
  #easyAnswer span div:last-child { border-radius:0 0 10px 10px; }
  #easyAnswer span div:nth-child(odd) { background-color:#00000077; }
  #easyAnswer span div:nth-child(even) { background-color:#000000aa; }
  #easyAnswer span div:before { content:counter(answer) '. '; counter-increment:answer; color:#00daff; }
  #easyAnswer span div:hover { color:white; cursor:pointer; }

  #ticketHistory article .saveThisAnswer { position:absolute; right:0; cursor:pointer; opacity:.3; transition:.5s; }
  #ticketHistory article .saveThisAnswer:hover { opacity:1; color:#0000ff; }

  .uk-notification > div {background:black; border-left:3px solid;}
  .uk-notification > div small {font-size:12px;}
</style>`;



class EasyAnswer {
  constructor() {
    this.ticketHistory = $('#ticketHistory');
    this.ticketAnswer = $('#ticketAnswer');
    this.textarea = this.ticketAnswer.find('#answerText');

    this.column = this.ticketAnswer.parent().parent();

    this.easyAnswerID = 'easyAnswer';
    this.saveAnswerClass = 'saveAnswer';
    this.storageName = 'support';
    this.startAnswerPrefix = '@';

    this._init();
  }

  _init() {
    $('head').eq(0).append(style);

    let storage = localStorage[this.storageName] || '{}';

    this.answer = (JSON.parse(storage).answer || []).filter(el => el);

    this.ea = $('<div>', {
      id: this.easyAnswerID,
      html: $('<span>', {html: this.answer.map(text => $('<div>', {name:text.toLowerCase().replace(/"/g, '\''), text:text}))}),
      style: 'display:none;', // width:${this.ticketAnswer.outerWidth() + 40 - 4}px;
    }).insertBefore(this.textarea.eq(0));

    this.ea.on('click contextmenu', 'div', event => this._easyAnswer_click(event));
    this.textarea.on('input propertychange', event => this._textarea_input(event));
    this.ticketHistory.on('mouseenter', 'article', event => this._save_mouseenter(event));

    return this;
  }


  _easyAnswer_click(event) {
    event.preventDefault();
    let elem = event.target;

    if (event.type == 'contextmenu') {
      if (confirm('Точно удалить следующий текст?\n\n'+ elem.textContent)) this.delete($(elem).index()).resize();
    }
    else if (event.type == 'click') {
      let value = this.textarea.val().trim().split(this.startAnswerPrefix);

      if (value.length <= 1) return;

      value.pop();
      value = value.join(this.startAnswerPrefix);

      this.ea.hide();
      this.textarea.val((value ? `${value}` : '') + elem.textContent +' ').focus();
    }
  }


  _textarea_input(event) {
    let inputText = event.target.value;
      if (inputText.indexOf(this.startAnswerPrefix) < 0) return this.ea.hide();

    let children = this.ea.find('> span >');
      if (!children.length) return this.ea.hide();

    let startAnswer = inputText.split(this.startAnswerPrefix).pop();

    if (startAnswer) {
      let convert = (str, eng) => {
        str = str.toLowerCase().replace(/"/g, '\'');
        let replacer = {
          "q":"й", "w":"ц", "e":"у", "r":"к", "t":"е", "y":"н", "u":"г", "i":"ш", "o":"щ", "p":"з", "[":"х", "]":"ъ",
          "a":"ф", "s":"ы", "d":"в", "f":"а", "g":"п", "h":"р", "j":"о", "k":"л", "l":"д", ";":"ж", "'":"э",
          "z":"я", "x":"ч", "c":"с", "v":"м", "b":"и", "n":"т", "m":"ь", ",":"б", ".":"ю", "/":".",
        };

        if (eng) str = str.replace(/[A-z/,.;\[\]\']/g, char => char.toLowerCase() == char ? replacer[char] : replacer[char.toLowerCase()].toUpperCase());
        return str;
      };

      let coincidental = children.hide().filter(`[name*="${convert(startAnswer)}"], [name*="${convert(startAnswer, true)}"]`).show();
      this.ea[coincidental.length ? 'show' : 'hide']();
    }
    else {
      this.ea.show();
      children.show();
    }

    this.resize();
  }


  _save_mouseenter(event) {
    let article = $(event.target);

    if (!article.is('article')) article = article.parents('article').eq(0);
    if (article.children(`.${this.saveAnswerClass}`).length) return;

    $('<div>', {
      text: 'Save ',
      class: this.saveAnswerClass,
      'uk-icon': 'icon:cloud-upload',
      click: event => this.save($(event.target).hide().siblings('.uk-comment-body').children(':first').html(), false),
    }).prependTo(article);
  }


  resize() {
    this.ea.outerWidth(this.column.outerWidth()).children('span').css('maxHeight', $(window).height() / 2);

    let left = this.textarea.offset().left - this.ea.outerWidth() - parseInt(this.ticketHistory.css('paddingLeft'));
    let top = this.textarea.offset().top + (this.textarea.outerHeight() / 2) - (this.ea.outerHeight() / 2);

    this.ea.offset({top:top, left:left});

    return this;
  }

  save(value) {
    if (!value) this.notification('danger', 'Ошибка сохранения', 'Входная строка пуста.');
    else {
      this.answer.push(value);
      this.ea.children().first().append($('<div>', {name:value.toLowerCase(), text:value}));
      localStorage[this.storageName] = JSON.stringify({'answer': this.answer});

      this.notification('success', 'Сохранено');
    }

    return this;
  }

  delete(id) {
    if (!this.answer[id]) this.notification('danger', 'Ошибка удаления', `Ответ с id="${id}" отсутствует в массиве из ${this.answer.length} ответов.`);
    else {
      delete this.answer[id];
      this.ea.find('div').eq(id).remove();
      localStorage[this.storageName] = JSON.stringify({'answer': this.answer.filter(el => el)});

      this.notification('success', 'Удалено');
    }

    return this;
  }


  notification(status, title, text = '') {
    UIkit.notification({message:`${title}<br><small>${text.length > 99 ? text.slice(0, 90) +'...' : text}</small>`, status:status, pos:'top-right'});
  }

}
