# EasyAnswer
Панель часто используемых ответов

Удобно было копировать-вставлять ответы из файла Word? Но пора двигаться дальше: вставлять ответы непосредственно из самого браузера, чтоб не переключаться между окнами. Представляю вам наконец-таки реализованную функцию EasyAnswer \o/


## Installation
```js
jQuery(document).ready(function($) {
    new EasyAnswer();
});
```


## Usage

| Command | Description |
| ------- | ----------- |
| `@`			| вызвать подсказку (если они уже заданы) |
| `ПКМ`		| удалить выбранную подсказку |
| `ЛКМ`		| вставить выбранную подсказку |


## Examples
> ![Main example](./images/example.png)

> ![Save button](./images/saveButton.png)


<br><hr>

<div align="center">Разработано для <a href="https://iPlayCraft.ru/">iPlayCraft.ru</a><br>2018 год</div>
